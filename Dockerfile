FROM openjdk
ARG profile
RUN yum install wget -y
RUN mkdir /var/certificates
RUN wget -P /var/certificates -U f https://iit.com.ua/download/productfiles/CACertificates.p7b
RUN mkdir /opt/sc2
RUN wget -P /opt/sc2 ftp://user-ftp:user@10.30.1.28/appeal-addons/*
RUN mv /opt/sc2/pb_3335400573.jks /Key-6.dat
RUN mv /opt/sc2/$profile /opt/sc2/scp.cfg
RUN wget -P /var/certificates ftp://user-ftp:user@10.30.1.28/certs/*
RUN chmod -R 777 /opt/sc2
ADD target/appeal-service.jar /app.jar
RUN echo -e "cd /opt/sc2\n./sc2psd.start.sh\necho \"127.0.0.1 apisrv.hsc.gov.ua\n127.0.0.1 apitest.hsc.gov.ua\" >> /etc/hosts\njava -jar -Dspring.profiles.active=$profile /app.jar " >> run.sh
RUN chmod +x run.sh
CMD ./run.sh
