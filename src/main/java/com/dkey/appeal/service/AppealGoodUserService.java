package com.dkey.appeal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class AppealGoodUserService {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private InternalAuthTokenService internalAuthTokenService;
    @Value("${internal.service.guser}")
    private String url;
    @Value("${internal.service.credentials.guser.username}")
    private String username;
    @Value("${internal.service.credentials.guser.password}")
    private String password;

    public ResponseEntity sendAppeal(String file, String token) {
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("ACCESS_KEY", String.valueOf(token));
        map.add("FILE", String.valueOf(file));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(
                this.url, request , String.class);
        return ResponseEntity.of(Optional.of(response.getStatusCode()));
    }

    public ResponseEntity sendSmpAppeal(String file) throws Exception{
        String token = internalAuthTokenService.getSmpToken(username, password);
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("ACCESS_KEY", String.valueOf(token));
        map.add("FILE", String.valueOf(file));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(
                this.url, request , String.class);
        return ResponseEntity.of(Optional.of(response.getStatusCode()));
    }
}
