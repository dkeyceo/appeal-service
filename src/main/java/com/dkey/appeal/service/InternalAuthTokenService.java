package com.dkey.appeal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class InternalAuthTokenService {
    @Autowired
    private RestTemplate restTemplate;
    @Value("${internal.service.auth}")
    private String url;

    public ResponseEntity getToken(String username, String password){
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("USERNAME", username);
        map.add("USERPASS",password);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(this.url, request , String.class);
        return response;
    }

    public String getSmpToken(String username, String password){
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("USERNAME", username);
        map.add("USERPASS",password);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<Map> responseEntity = restTemplate.exchange(url.toString(), HttpMethod.POST,
                request, Map.class);
        String token = responseEntity.getBody().get("ACCESS_KEY").toString();
//        ResponseEntity<Map<String, String>> response = restTemplate.postForEntity(this.url, request , Map.class);

        return token;
    }
}
