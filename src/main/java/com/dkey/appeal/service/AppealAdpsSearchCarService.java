package com.dkey.appeal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class AppealAdpsSearchCarService {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private VerificationSignService verificationSignService;
    public Map<String, Object> verifySign(Map requestEncoded) throws Exception {
        return verificationSignService.verifySign(requestEncoded);
    }
    public Map<String, String> paramExtractor(Map requestDecoded){
        HashMap params = new HashMap();
        params.put("licensePlate", requestDecoded.get("LICENSEPLATE"));
        params.put("REQUESTID", requestDecoded.get("REQUESTID"));
        return params;
    }
    public Map<String, Object> getAdpsCard(Map requestEncoded) throws Exception {
        Map<String, Object> verifiedRequest= verifySign(requestEncoded);
        Map <String, String > params = paramExtractor(verifiedRequest);
        ResponseEntity<Map> responseEntity = restTemplate.getForEntity("http://aggregator-service:8170/adps/?licensePlate={licensePlate}", Map.class, params.get("licensePlate"));
        responseEntity.getBody().put("REQUESTID", params.get("REQUESTID"));
        responseEntity.getBody().put("RESPONSEID", UUID.randomUUID());
        return responseEntity.getBody();
    }
}
