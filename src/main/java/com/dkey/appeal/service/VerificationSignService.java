package com.dkey.appeal.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class VerificationSignService {

    private static final String verificationUrl = "http://verification-digital-sign:8160/verify";
    private RestTemplate restTemplate;
    public VerificationSignService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Map<String, Object> verifySign(Map<String, Object> requestForVerification) throws Exception{
        ResponseEntity<Map> responseEntity = restTemplate.postForEntity(verificationUrl, requestForVerification, Map.class);
        Map<String, Object> nodes = responseEntity.getBody();
        if(nodes.get("result").equals(true)){
            return (Map<String, Object>) nodes.get("body");
        }
        throw new Exception("Signature not recognized!");
    }
}
