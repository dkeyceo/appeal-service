package com.dkey.appeal.controllers;

import com.dkey.appeal.service.AppealGoodUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AppealGoodUserController {

    @Autowired
    private AppealGoodUserService appealGoodUserService;

    @RequestMapping(value = "${suffix.appeal.guser.send}",method = RequestMethod.POST,consumes = {"application/json"})
    public ResponseEntity sendAppeal(@RequestBody String file, @PathVariable("token") String token) {

        try {
            return appealGoodUserService.sendSmpAppeal(file);
        }catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.badRequest().body("Invalid request!");
        }
    }

}
