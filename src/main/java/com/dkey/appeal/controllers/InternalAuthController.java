package com.dkey.appeal.controllers;

import com.dkey.appeal.service.InternalAuthTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InternalAuthController {
    @Autowired
    private InternalAuthTokenService internalAuthToken;
    @RequestMapping(value = "${suffix.auth.url}", method = RequestMethod.POST)
    public ResponseEntity getToken(@PathVariable("user") String username, @PathVariable("password") String password){
        return internalAuthToken.getToken(username, password);
    }
}
