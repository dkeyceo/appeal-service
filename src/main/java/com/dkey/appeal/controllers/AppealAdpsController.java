package com.dkey.appeal.controllers;

import com.dkey.appeal.service.AppealAdpsSearchCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
public class AppealAdpsController {

    @Autowired
    private RestTemplate restTemplate;
    private final AppealAdpsSearchCarService appealAdpsSearchCarService;

    @Autowired
    public AppealAdpsController(AppealAdpsSearchCarService appealAdpsSearchCarService) {
        this.appealAdpsSearchCarService = appealAdpsSearchCarService;
    }

    @RequestMapping("/appeal/adps/getcarinfo")
    public ResponseEntity<Map> getCarInfo(@RequestBody Map<String, Object> requestParam) {
        HashMap error = new HashMap();
        try {
            return ResponseEntity.ok(appealAdpsSearchCarService.getAdpsCard(requestParam));
        } catch (Exception e) {
            e.printStackTrace();
            error.put("error 1", "signature is not recognized");
        }
        return ResponseEntity.ok(error);
    }
}
